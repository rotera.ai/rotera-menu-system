/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {createMenuSystem} from '../src';
import {Menu, MenuSystem, MenuType} from '../src/types/menuTypes';
import {whisper} from '@oliveai/ldk';
import {assertComponentIsLink} from './componentAssertions';
import {Link, Whisper} from '@oliveai/ldk/dist/whisper';

const ourTab: Menu = {
  id: 'MENU',
  name: 'Menu',
  keywords: [],
  type: MenuType.CLICKABLE_MENU_LIST,
  description: 'Test description',
  items: ['ANOTHER_TAB', 'RANDOM'],
};

const anotherTab: Menu = {
  id: 'ANOTHER_TAB',
  name: 'Another Menu',
  keywords: [],
  type: MenuType.CLICKABLE_MENU_LIST,
  description: 'Another Test description',
  items: ['MENU'],
};

const customRenderMock = jest.fn();
const customTab: Menu = {
  id: 'CUSTOM',
  name: 'Custom Menu',
  keywords: [],
  type: MenuType.CUSTOM,
  render: customRenderMock,
};

let tabSystem: MenuSystem;
let whisperCreateMock: jest.SpyInstance;
beforeAll(() => {
  tabSystem = createMenuSystem([ourTab, customTab, anotherTab]);
  whisperCreateMock = jest.spyOn(whisper, 'create').mockImplementation();
});

beforeEach(() => {
  whisperCreateMock.mockClear();
});

describe('Menu System', () => {
  /**
   * We should be able to retrieve a tab object given its ID
   */
  test('Can retrieve tab by id', () => {
    expect(tabSystem.getMenuForId(ourTab.id)).toEqual(ourTab);
    expect(tabSystem.getMenuForId(anotherTab.id)).toEqual(anotherTab);
  });

  /**
   * We should return undefined if a tab does not exist
   */
  test('Undefined return if no tab found', () => {
    expect(tabSystem.getMenuForId('RANDOM')).toEqual(undefined);
  });

  /**
   * Opening unknown tab does not throw
   */
  test('Opening undefined does not throw', () => {
    expect(tabSystem.openMenu('RANDOM'));
  });

  /**
   * We should be able to open a menu by ID
   */
  test('Can open tab by ID', async () => {
    await tabSystem.openMenu(ourTab.id);
    expect(whisperCreateMock).toBeCalled();
    const createdComponents = whisperCreateMock.mock.calls[0][0].components;
    expect(createdComponents).toHaveLength(2);
    expect(createdComponents[0]).toMatchInlineSnapshot(`
      Object {
        "body": "Test description",
        "type": "markdown",
      }
    `);
    const link = createdComponents[1];
    assertComponentIsLink(link);
    expect(link.text).toEqual(anotherTab.name);
  });

  /**
   * We should be able to open a menu by ID
   */
  test('Can open custom tab by ID', async () => {
    await tabSystem.openMenu(customTab.id);
    expect(customRenderMock).toBeCalled();
  });

  /**
   * We should be able to open another tab via clicking
   */
  test('Can open tab by clicking link in clickable tab', async () => {
    await tabSystem.openMenu(ourTab.id);
    expect(whisperCreateMock).toBeCalled();
    const createdComponents = whisperCreateMock.mock.calls[0][0].components;
    const link = createdComponents[1] as Link;
    expect(link.onClick).toBeTruthy();
    await link.onClick?.(undefined, {} as Whisper);
    const createdComponents2 = whisperCreateMock.mock.calls[1][0].components;
    expect(createdComponents2).toMatchInlineSnapshot(`
      Array [
        Object {
          "body": "Another Test description",
          "type": "markdown",
        },
        Object {
          "onClick": [Function],
          "text": "Menu",
          "type": "link",
        },
      ]
    `);
  });
});
