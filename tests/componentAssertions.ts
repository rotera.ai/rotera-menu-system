/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {whisper} from '@oliveai/ldk';
import {Component, WhisperComponentType} from '@oliveai/ldk/dist/whisper';

function assertComponentType(
  whisper: Component,
  expectedType: WhisperComponentType
): void {
  if (whisper.type !== expectedType) {
    throw `Expected whisper type '${expectedType}', got '${whisper.type}'`;
  }
}

export function assertComponentIsMessage(
  component: Component
): asserts component is whisper.Message {
  return assertComponentType(component, WhisperComponentType.Message);
}

export function assertComponentIsMarkdown(
  component: Component
): asserts component is whisper.Markdown {
  return assertComponentType(component, WhisperComponentType.Markdown);
}

export function assertComponentIsLink(
  component: Component
): asserts component is whisper.Link {
  return assertComponentType(component, WhisperComponentType.Link);
}

export function assertComponentIsBox(
  component: Component
): asserts component is whisper.Box {
  return assertComponentType(component, WhisperComponentType.Box);
}

export function assertComponentIsDivider(
  component: Component
): asserts component is whisper.Divider {
  return assertComponentType(component, WhisperComponentType.Divider);
}

export function assertComponentIsListPair(
  component: Component
): asserts component is whisper.ListPair {
  return assertComponentType(component, WhisperComponentType.ListPair);
}

export function assertComponentIsCollapseBox(
  component: Component
): asserts component is whisper.CollapseBox {
  return assertComponentType(component, WhisperComponentType.CollapseBox);
}

export function assertComponentIsButton(
  component: Component
): asserts component is whisper.Button {
  return assertComponentType(component, WhisperComponentType.Button);
}
