/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */
import {whisper} from '@oliveai/ldk';
import {Message} from '@oliveai/ldk/dist/whisper';
import lang from '../src/static/i18n/lang';
import * as menuSystem from '../src/menuSystem';
import {flushPromises} from './testUtils';

let whisperCreateMock: jest.SpyInstance;
const closeMock = jest.fn(callback => callback());
beforeAll(() => {
  // returns a resolved mock whisper
  whisperCreateMock = jest.spyOn(whisper, 'create').mockResolvedValue({
    id: '',
    close: closeMock,
    update: jest.fn(),
    componentState: new Map(),
  });
});

beforeEach(() => {
  whisperCreateMock.mockClear();
  closeMock.mockClear();
});

/**
 * Tests for presentPreloadAndWaitForTabToLoad() in menuSytem.ts
 */
describe('Preload System', () => {
  /**
   * We should be able to run the function after data is loaded
   */
  test('Preloads and Waits for Data to be loaded into a fake datastore', async () => {
    jest.useFakeTimers();
    let resolved: string | undefined = undefined;
    const shouldResolve = {
      should: false,
    };
    const isResolvedTrue = jest.fn(() => {
      if (shouldResolve.should) {
        // We should resolve return something
        return 'We resolved yay!';
      } else {
        // We should NOT resolve, return undefined as expected from an empty datastore
        return undefined;
      }
    });
    menuSystem
      .presentPreloadAndWaitForTabToLoad('test 1', isResolvedTrue)
      .then(response => {
        resolved = response;
      });
    // resolve promise of loading whisper to hit .then block
    await flushPromises();
    jest.advanceTimersByTime(600);
    expect(resolved).toEqual(undefined);
    shouldResolve.should = true;
    jest.advanceTimersByTime(600);
    // resolve promise of presentPreloadAndWaitForTabToLoad
    await flushPromises();
    expect(closeMock).toBeCalledTimes(1);
    expect(resolved).toEqual('We resolved yay!');
    jest.useRealTimers();
  });

  /**
   * We should resolve the results immedeiately if possible
   */
  test('Should resolve immediately if response from checkfunction is defined', async () => {
    jest.useFakeTimers();
    let isDefined = false;
    // creates callback function that will return true
    const checkFunction = jest.fn(() => {
      return true;
    });
    menuSystem
      .presentPreloadAndWaitForTabToLoad('test 2', checkFunction)
      // sets test to the response of checkFunction
      .then(response => (isDefined = response));
    jest.advanceTimersByTime(600);
    await flushPromises();
    expect(isDefined).toEqual(true);
    // loading whisper is not created
    expect(whisperCreateMock).toBeCalledTimes(0);
    jest.useRealTimers();
  });

  /**
   * we should display a whisper with a loading message
   */
  test('Should display a loading whisper that calls clearInterval when closed', async () => {
    jest.useFakeTimers();
    menuSystem.presentPreloadAndWaitForTabToLoad('test 3', jest.fn());
    const loadingWhisper = whisperCreateMock.mock.calls[0][0];
    expect(whisperCreateMock).toBeCalled();
    // checks that label matches provided string
    expect(loadingWhisper.label).toEqual('test 3');
    // checks loading message matches the translated constant
    expect((loadingWhisper.components[0] as Message).body).toEqual(
      lang.ui.loading
    );
    // clearInterval is not called yet
    expect(clearInterval).toBeCalledTimes(0);
    // closes loading whisper
    loadingWhisper.onClose();
    // checks that clearInterval was called when whisper was closed
    expect(clearInterval).toBeCalledTimes(1);
    jest.useRealTimers();
  });
});
