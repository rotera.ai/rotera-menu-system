# Rotera Menu System

This package allows you to easily define menu trees in Olive Helps.

Menus
-----

In the Rotera Menu System, all whispers are generated via a Menu framework.

There are currently two menu types:

* ClickableMenuList - A menu that shows a list of sub-menus, clicking a link will open the named sub-menu

  ```
    const ourTab: Menu = {
        id: 'MENU',
        name: 'Menu',
        keywords: [],
        type: MenuType.CLICKABLE_MENU_LIST,
        description: 'Test description',
        items: ['ANOTHER_TAB', 'RANDOM'], //<-- These sub tabs will be shown in the auto-generated menu whisper
    };  
  ```
* Custom - A menu that exposes a render function that allows an implementer to dictate behavior when the tab is "opened"

  ```
    const customTab: Menu = {
        id: 'CUSTOM',
        name: 'Custom Menu',
        keywords: [],
        type: MenuType.CUSTOM,
        render: (context) => console.log("Hello World "+contxt), //<--- Prints Hello World to the console on open
    };
  ```

Menu System functions
---------------------

`openMenu(tabId: string, context?: unknown)` allows you to open a menu by its ID, optionally you can pass some context to the tab.


Example: `openMenu("CUSTOM", "Bill")` using the customTab defined above would print "Hello World Bill" to the console.

`getMenuForId(menuId: string)` returns a tab if one exists that matches the menu id, otherwise it returns undefined.
