/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

// TODO: If we end up implementing localization then we'll use an exiting i18n library
//  and depending on the library we may end up converting the `static/<lang>.ts` files to JSON, though the structure will stay the same.
//  We may also want to look into turning this into a global var so we don't need to include `lang` in every file.
import en from './en';

export default en;
