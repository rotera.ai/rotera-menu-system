/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {whisper} from '@oliveai/ldk';
import {Menu, MenuSystem, MenuType} from './types/menuTypes';
import {renderClickableMenuListMenu} from './menus/clickableListMenu';
import {WhisperComponentType} from '@oliveai/ldk/dist/whisper/types';
import lang from './static/i18n/lang';

/**
 * Create a menu system that will track a series of menus
 * @param menuList list of menus in the system
 */
export function createMenuSystem(menuList: Menu[]): MenuSystem {
  function getMenuById(menuId: string) {
    return menuList.find(menu => menu.id === menuId);
  }

  async function openTab(menuId: string, context?: unknown) {
    const menu = getMenuById(menuId);
    if (menu) {
      switch (menu.type) {
        case MenuType.CLICKABLE_MENU_LIST: {
          if (menuSystem) await renderClickableMenuListMenu(menuSystem, menu);
          break;
        }
        case MenuType.CUSTOM: {
          await menu.render(context);
          break;
        }
      }
    } else {
      console.error(`Could not find tab by id [${menuId}]`);
    }
  }

  const menuSystem: MenuSystem = {
    getMenuForId: getMenuById,
    openMenu: openTab,
  };

  return menuSystem;
}

/**
 * @name presentPreloadAndWaitForTabToLoad
 * @description Preloads and Waits for Data to be loaded from Excel file
 *              calls a loading whisper that is displayed until data is loaded
 * @param label string used for label of loading whisper
 * @param checkFunction function that retruns the results that are being waited for
 * @returns Promise<T>
 */
export async function presentPreloadAndWaitForTabToLoad<T>(
  label: string,
  checkFunction: () => T | undefined
): Promise<T> {
  let createdWhisper: whisper.Whisper;
  return new Promise<T>(resolve => {
    // checks if callback results is ready immediately
    const initialResult = checkFunction();
    if (initialResult !== undefined) {
      // resolve the data
      return resolve(initialResult);
    }

    // create timer that checks for data
    const interval = setInterval(async () => {
      // checks if data is ready
      const result = checkFunction();
      if (result !== undefined) {
        // clears the timer
        clearInterval(interval);
        // closes loading whisper
        createdWhisper?.close(() => {});
        // resolve the data
        resolve(result);
      }
    }, 500);

    // creates loading whisper
    whisper
      .create({
        label: label,
        onClose: () => {
          // clear timer when closed
          clearInterval(interval);
        },
        components: [
          {
            type: WhisperComponentType.Message,
            body: lang.ui.loading,
          },
        ],
      })
      .then(whisper => {
        // when loading whisper is resolved assign the new whisper
        createdWhisper = whisper;
      });
  });
}
