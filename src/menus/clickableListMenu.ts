/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {ClickableListMenu, MenuSystem} from '../types/menuTypes';
import {whisper} from '@oliveai/ldk';
import {Component, WhisperComponentType} from '@oliveai/ldk/dist/whisper';
import lang from '../static/i18n/lang';

/**
 * Render function for clickable menu list
 */
export async function renderClickableMenuListMenu(
  menuSystem: MenuSystem,
  fromMenu: ClickableListMenu
) {
  const items = fromMenu.items;
  const buttons = items
    .map<Component | undefined>(clickableItem => {
      const actualItem =
        typeof clickableItem === 'string'
          ? {destinationMenuId: clickableItem}
          : clickableItem;
      const menu = menuSystem.getMenuForId(actualItem.destinationMenuId);
      if (!menu) {
        console.warn(
          `No menu registered for id [${actualItem.destinationMenuId}]`
        );
        return;
      }
      return {
        text: menu.name,
        type: whisper.WhisperComponentType.Link,
        onClick: () => {
          menuSystem.openMenu(menu.id, actualItem.context);
        },
      };
    })
    .filter((item): item is Component => !!item);

  await whisper.create({
    label: fromMenu.menuTitleOverride ?? fromMenu.name,
    onClose: () => {},
    components: [
      {
        type: WhisperComponentType.Markdown,
        body: fromMenu.description
          ? fromMenu.description
          : lang.ui.menu_list_default_description,
      },
      ...buttons,
    ],
  });
}
