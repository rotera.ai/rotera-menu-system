/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

export enum MenuType {
  CUSTOM,
  CLICKABLE_MENU_LIST,
}

export interface BaseMenu<T extends MenuType> {
  id: string;
  type: T;
  name: string;
  keywords: string[];
  menuTitleOverride?: string;
}

export type CustomMenu = BaseMenu<MenuType.CUSTOM> & {
  render: (context?: unknown) => Promise<void> | void;
};

export type ClickableItem = {
  destinationMenuId: string;
  context?: string;
};

export type ClickableListMenu = BaseMenu<MenuType.CLICKABLE_MENU_LIST> & {
  items: (string | ClickableItem)[];
  description?: string;
};

export type Menu = CustomMenu | ClickableListMenu;

export type MenuSystem = {
  getMenuForId: (tabId: string) => Menu | undefined;
  openMenu: (tabId: string, context?: unknown) => Promise<unknown>;
};
